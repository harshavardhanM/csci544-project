import sys,glob,nltk, argparse, io, random, re
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import RegexpTokenizer
from sklearn.externals import joblib
import idf as idf
from nltk.tokenize import sent_tokenize

#Function to calulate IDF for a sentence in form of list of words
#input: words (list of string), idf_model (dict)
#output: idf of sentence (int)
def cal_idf(words,idf_model,n):
	sent_idf = 1
	word_count = 0.0
	#calculate IDF for a sentence.
	for i in range(1,n+1):
		ngramwords = idf.ngram(words,i)
		for word in ngramwords:
			if word in idf_model:
				sent_idf += idf_model[word]
		word_count += len(ngramwords)
	sent_idf = sent_idf/word_count
	return sent_idf

def cleaning_sent(sent):
	paren = re.compile(r'(.+?)\(.*?\)(.+?)')
	sent = paren.sub(r'\1 \2',sent)
	sent = re.sub(r'\s+',' ', sent).strip()
	return sent

def select_clue(idf_model,n=1,infiles=None,num_sent=5,show_debug=False,show_all=False,num_para=5,show_random=False):
	tokenizer = RegexpTokenizer(r'\w+')
	stemmer = PorterStemmer()
	#if there is input file
	if(infiles != None):
		return_str = []
		for infile in infiles:
			print('Calculating IDF of input file: ' + infile)
			f = open(infile,'r',errors='ignore')
			#skip first two lines which are topic and category
			f.readline()
			f.readline()
			#split it by paragragh and take only first n paragraphs
			raw = f.read().split('\n')
			if(len(raw)< num_para):
				print('The input file is too small.')
				return 0
			#tokenize data to be sentences
			sentence_list = sent_tokenize(' '.join(raw[0:num_para]))
			sentence_list = [cleaning_sent(sent) for sent in sentence_list]
			idf_sent_list = []
			filtered_sentence_list = [s for s in sentence_list]
			for sent in sentence_list:
				#tokenize sentence to word and do pos tagging
				words = tokenizer.tokenize(sent)
				word_tags = nltk.pos_tag(words)
				verb_flag = 0
				#remove sentence that is too short or does not have any verb.
				for word_tag in word_tags:
					if('VB' in word_tag[1]):
						verb_flag = 1
						break
				if(verb_flag == 0 or len(words) < 4):
					filtered_sentence_list.remove(sent)
				else:
					#calculate idf of each sentences.
					words = idf.lowercase(words)
					words = idf.stem_tokens(words,stemmer)
					sent_idf = cal_idf(words, idf_model, n)
					idf_sent_list.append(sent_idf)
			print('Finish calculating.')
			#select all sentences
			if(show_all):
				num_sent = len(filtered_sentence_list)
			#select sentences randomly
			if(show_random):
				random.seed()
				random_list = random.sample(list(zip(filtered_sentence_list,idf_sent_list)),num_sent) 
				sorted_lists = sorted(random_list, reverse=True, key=lambda x: x[1])
				str_out = [pair[0] for pair in sorted_lists]
			#select sentence randomly based on range of idf value
			else:
				sorted_lists = sorted(zip(filtered_sentence_list,idf_sent_list), reverse=True, key=lambda x: x[1])
				num_range =  [i for i in range(0,len(sorted_lists),round(len(sorted_lists)/num_sent))]
				num_range.append(len(sorted_lists))
				str_out = []
				for i in range(0,len(num_range)-1):
					str_out.append(random.sample(sorted_lists[num_range[i]:num_range[i+1]],1)[0])
			#print out sentences
			for sent in str_out:
				print(sent[0])
			return_str += str_out
		return return_str
	elif(show_debug):
		print('Enter Sentence:')
		input_stream = io.TextIOWrapper(sys.stdin.buffer, errors='ignore')
		#input from command line
		for line in input_stream:
			line = line.strip()
			if(len(line)>0):
				#tokenize sentence
				words = tokenizer.tokenize(line)
				#change all words to lower case
				words = idf.lowercase(words)
				#stem all words
				words = idf.stem_tokens(words,stemmer)
				sent_idf = cal_idf(words,idf_model,n)
				print(str(sent_idf))
def main():
	#read arguments
	parser = argparse.ArgumentParser(description='Calculate IDF for each sentence')
	parser.add_argument('idf_model', metavar='idf_model_file', type=str, help='an idf_model files')
	parser.add_argument('-n','--ngram', metavar='N', type=int,  default=1,help='ngram number (Default=1)')
	parser.add_argument('-i','--input_file',metavar='in_file', nargs='+', type=str, help='input file to calculate IDF')
	parser.add_argument('-m','--num_sent',metavar='num_sentence',type=int,default=5,help='number of sentence to be selected (Default=5)')
	parser.add_argument('-d','--debug',action='store_true', default=False, help='Debug mode (Default=False)')
	parser.add_argument('-a','--all',action='store_true',default=False,help='Show all sentences (Default=False)')
	parser.add_argument('-p','--paragraph',metavar='num_para',type=int,default=5,help='number of paragraph are reading from input file')
	parser.add_argument('-r','--random',action='store_true',default=False, help='Randome select the clues')
	args = parser.parse_args()
	n = args.ngram
	infiles = args.input_file
	num_sent = args.num_sent
	num_para = args.paragraph
	print('loading idf_model...')
	#load idf_model
	idf_model = joblib.load(args.idf_model)
	print('Finish loading.')
	select_clue(idf_model,n,infiles,num_sent,args.debug,args.all,num_para,args.random)
	
if __name__ == '__main__':
	main()