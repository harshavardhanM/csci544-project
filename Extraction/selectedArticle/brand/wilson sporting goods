Wilson Sporting Goods
brand
The Wilson Sporting Goods Company is an American sports equipment manufacturer based in Chicago, Illinois. Since 1989, it has been a foreign subsidiary of the Finnish group Amer Sports.
Wilson makes equipment for many sports, among them badminton, baseball, basketball, softball, American football, golf, racquetball, soccer, squash, tennis, and volleyball.
History.
The company traces its roots to the Schwarzchild & Sulzberger company (later changed to Sulzbeger & Son's) based in New York City that operated meat packing plants in New York, Chicago and Kansas City.
Sulzberger founded the Ashland Manufacturing Company in 1913 to use animal by-products from its slaughterhouses. It started out making tennis racket strings, violin strings, and surgical sutures but soon expanded into baseball shoes and tennis racquets.
In 1915, Thomas E. Wilson, former president of meatpacker Morris & Company, was appointed President and renamed the company Thomas E. Wilson Company. The company acquired the Hetzinger Knitting Mills to produce athletic uniforms and a caddie bag company which produced golf balls but soon expanded into footballs and basketballs.
In 1918, Wilson left to concentrate on the beef-packing business, changing the Sulzberger company to Wilson & Co. (which would ultimately become Iowa Beef Packers and then be taken over by Tyson Foods). The packing company continued to have control in the company until 1966 when it was sold to LTV.
Under new president L. B. Icely it acquired the Chicago Sporting Goods Company and struck a deal to supply the Chicago Cubs. It also hired Arch Turner, a leather designer who would design the leather football.
In 1922, it introduced the Ray Schalk catcher's mitt which became the standard. It worked with Knute Rockne to introduce the double-lined leather football and first valve football and the first waist-line football pants with pads.
In 1925, it was renamed Wilson-Western Sporting Goods following a distribution agreement with Western Sporting Goods.
After Rockne's death, the company focused on golf, introducing the R-90, a sand wedge golf club inspired by Gene Sarazen's victory in the 1932 British Open.
In 1931, it renamed itself Wilson Sporting Goods Company. During World War II it introduced the Wilson Duke football, featuring the best leather, ends that were hand-sewn, lock-stitch seams, and triple lining, which was adopted as the official ball of the National Football League.
Horween Leather Company has supplied Wilson with pebbled cowhide since 1941. Wilson is Horween Leather Company's largest customer, using the company's leather in manufacturing footballs and basketballs.
After the war it focused on tennis and signed Jack Kramer who developed its line of Jack Kramer signed tennis rackets.
Icley died in 1950 but the company continued to expand with many believing that Icely's introduction of a computer to monitor inventory had been a huge help. In 1955, it acquired Ohio-Kentucky Manufacturing for making footballs. In 1964 it acquired Wonder Products Company, which made toys and custom-molded items. It transformed the custom-mold section to make protective equipment in football and baseball, such as face masks for football helmets and leg guards for baseball catchers.
In 1967, it was acquired by Ling-Temco-Vought (LTV Corporation). In 1970 it was acquired by PepsiCo. It sold the official balls of the National Basketball Association and National Football League, and provided most of the uniforms of teams in Major League Baseball, United States Summer Olympics teams.
In 1971, the grandson of Thomas Wilson rejoined the executive team for Wilson, operating as director of North American Operations. Justin Wilson's corporate office is based in the Cincinnati area. The family business reaffirmed its regional presence from 1994–2001, during which time Mick Wilson, the current beneficiary of the Wilson Franchise, made himself prominently known throughout the Ohio River Valley Region. The Wilson family's active involvement continues to leave its legacy to this day.
In 1979, Wilson tennis balls were first used in the US Open, and still are used to this day. In 2006, the Australian Open began using Wilson Tennis Balls.
In 1985, it was acquired by Westray Capital Corporation through subsidiary WSGC Holdings.
In 1989, WSGC merged with Bogey Acquisitions Company, which is affiliated with the Finnish group Amer Sports.
Products and sponsorship.
American Football.
Former teams.
Many teams of the NFL have worn uniforms provided by Wilson, such as:
Baseball.
Wilson makes a variety of baseball gloves for several different patterns: pitcher, catcher, infield, outfield, and first base. Wilson's best known baseball glove models include the A2000, A2K, and A3000.
Golf.
Wilson Staff is the golf division of Wilson Sporting Goods. The company designs and manufactures a full range of golf equipment, accessories and clothing using the Wilson Staff, ProStaff and Ultra brands.
Many of the worlds top professional golfers have used Wilson equipment including Nick Faldo, Arnold Palmer and Ben Crenshaw; the latter two of whom used Wilson 8802 putters. Crenshaw even received the moniker "Little Ben" due to his proficiency with it. Current Wilson Staff players include Open and USPGA champion Pádraig Harrington.
Tennis.
Former players.
The original kevlar Pro Staff model (Sampras' racquet) was heavy (more than 350g strung) and small-headed (85 sq. in.); Roger Federer also used the same racquet model. Currently, he uses the BLX Pro Staff Six.One 90 model that is heavy (339 g/12 oz unstrung) and slightly larger (90 sq. in.). Jim Courier and Stefan Edberg also used the Pro Staff Original, Edberg later switching to the Pro Staff Classic in 1991, which was the same racquet (85 sq. in. with slightly rounded frame edges) but with different paint work. In late 2009, Wilson unveiled their latest line of racquets, codenamed 20x, which they would later rename BLX. This line directly replaces their previous K-Factor series with all new technologies. Also, many pros use custom-made racquets that perform differently from the mass-produced versions.
Aside from tennis racquets, the Wilson sporting goods company also makes tennis shoes, balls, strings, clothes, and racquet bags.
In popular culture.
A Wilson volleyball "co-starred" alongside Tom Hanks in the film "Cast Away", and Tom's character named the ball "Wilson" in the film. After the success of the film, Wilson Sporting Goods actually created and marketed volleyballs with Wilson's "face" printed on it. Though no longer available as regular store merchandise, the balls have remained listed on the Wilson Sporting Goods website.
Hanks' character's loss of the famed volleyball is lampooned in a scene in the film "Behind Enemy Lines" when the steam-catapult aboard an aircraft carrier launches a Wilson football off the flight deck prompting Owen Wilson's character to cry "Wilson!"
External links.
Other
