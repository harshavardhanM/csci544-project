Annie's Homegrown
brand
Annie's Homegrown is a Berkeley, California-based maker of natural and organic pastas, meals and snacks. The company was founded in Hampton, Connecticut by Annie Withey and Andrew Martin, who previously founded Smartfood popcorn along with Ken Martin. It is perhaps best known for its macaroni and cheese product line, which comes in shell form and bunny rabbit shapes, and is the second best selling macaroni and cheese in the United States. Their mascot is a rabbit named Bernie, who appears in the seal of approval called the "Rabbit of Approval". The company also produces Annie's Naturals, which consists of condiments, dressings, and barbecue sauces. In January 2012, Annie's introduced a line of certified organic rising crust frozen pizzas.
History.
Annie Withey co-founded Annie's Homegrown with Andrew Martin in 1989. Initially, the company only sold natural macaroni and cheese, in New England supermarkets.
In 1995 Annie's completed a direct public offering that raised $1.3 million.
In 1999, John Foraker, an owner of Homegrown Natural Foods, which made flavored olive oils and mustards, and his company invested $2 million in Annie's. An agreement was reached that would buy out Withey and Martin's shares in the company and make Annie's a private company. Withey became Annie's "inspirational president", and the company began distributing its products to chains like Costco, Kroger, and Safeway.
In 2002, Solera Capital became the majority investor in the company with $23 million. They also added Foraker's company to the business, and moved Annie's headquarters from Boston to Berkeley, California.
In 2005, the company bought out Annie's Naturals, an organic salad dressing and condiment company founded by Annie Christopher of North Calais, Vermont.
In January 2012, Annie's announced the introduction of a certified organic rising crust frozen pizza line, the first of its kind. Annie's certified organic pizza line is exclusive to Whole Foods Market.
On January 22, 2013, the company initiated a voluntary recall of Annie's Homegrown Frozen Pizza due to the possible presence of fragments of flexible metal mesh caused by a faulty screen at a third-party flour mill. Annie's initiated its voluntary recall as a precaution because the pieces of the fine wire were too small to be detected by the comprehensive metal control programs in place. On February 4, 2013, Annie's announced that its Rising Crust Frozen Pizzas would begin shipping again to stores nationwide on February 11, 2013.
On April 3, 2014 Annie's opened their doors to their first bakery manufacturing plant, purchased from Safeway Inc. and located in Joplin, Missouri, that currently has an estimated 100 employees. 
Becoming a publicly traded company.
The majority of stock of Annie's was owned by Solera Capital, LLC. In December 2011, Annie's filed with the SEC to raise up to $100 million in an initial public offering.
Purchase.
General Mills bought Annie's on 8 September 2014.
