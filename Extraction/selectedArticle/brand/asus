Asus
brand
ASUSTeK Computer Inc. (), usually referred to as ASUS () and in Chinese Huáshuò (), and trading under that name, is a Taiwanese multinational computer hardware and electronics company headquartered in Beitou District, Taipei, Taiwan. Its products include desktops, laptops, netbooks, LED/LCD panels, mobile phones, networking equipment, monitors, motherboards, graphics cards, optical storage, multimedia products, servers, workstations, and tablet PCs. The company's slogan/tagline was "Inspiring Innovation. Persistent Perfection." and is currently "In Search of Incredible."
ASUS is the world's fifth-largest PC vendor by 2014 unit sales (after Lenovo, HP, Dell and Acer). ASUS appears in "BusinessWeek"’s "InfoTech 100" and "Asia’s Top 10 IT Companies" rankings, and it ranked first in the IT Hardware category of the 2008 Taiwan Top 10 Global Brands survey with a total brand value of $1.3 billion.
ASUS has a primary listing on the Taiwan Stock Exchange and a secondary listing on the London Stock Exchange.
Name.
According to the company website, the name "ASUS" originates from "Pegasus", the winged horse of Greek mythology. Only the last four letters of the word were used in order to give the name a high position in alphabetical listings.
History.
ASUS was founded in Taipei in 1989 by T.H. Tung, Ted Hsu, Wayne Hsieh and M.T. Liao, all four having previously worked at Acer as computer engineers. At this time, Taiwan had yet to establish a leading position in the computer-hardware business. Intel Corporation would supply any new processors to more established companies like IBM first, and Taiwanese companies would have to wait for approximately six months after IBM received their engineering prototypes. According to the legend, the company created a prototype for a motherboard using an Intel 486, but it had to do so without access to the actual processor. When ASUS approached Intel to request a processor to test it, Intel itself had a problem with their motherboard. ASUS solved Intel's problem and it turned out that ASUS' own motherboard worked correctly without the need for further modification. Since then, ASUS was receiving Intel engineering samples ahead of its competitors.
In September 2005 ASUS released the first PhysX accelerator card. In December 2005 ASUS entered the LCD TV market with the TLW32001 model. In January 2006 ASUS announced that it would cooperate with Lamborghini to develop the VX series.
On 9 March 2006 ASUS was confirmed as one of the manufacturers of the first Microsoft Origami models, together with Samsung and Founder Technology.
On 8 August 2006 ASUS announced a joint venture with Gigabyte Technology.
On 5 June 2007 ASUS announced the launch of the Eee PC at COMPUTEX Taipei.
On 9 September 2007 ASUS indicated support for Blu-ray, announcing the release of a BD-ROM/DVD writer PC drive, BC-1205PT. ASUS subsequently released several Blu-ray based notebooks.
In January 2008, ASUS began a major restructuring of its operations, splitting into three independent companies: ASUS (focused on applied first-party branded computers and electronics); Pegatron (focused on OEM manufacturing of motherboards and components); and Unihan Corporation (focused on non-PC manufacturing such as cases and molding). In the process of the restructuring, a highly criticized pension-plan restructuring effectively zeroed out the existing pension balances. The company paid out all contributions previously made by employees.
On 9 December 2008, the Open Handset Alliance announced that ASUS had become one of 14 new members of the organization. These "new members will either deploy compatible Android devices, contribute significant code to the Android Open Source Project, or support the ecosystem through products and services that will accelerate the availability of Android-based devices."
On 1 June 2010, ASUS spun off Pegatron Corp.
In October 2010, ASUS and Garmin announced that they would be ending their smartphone partnership as a result of Garmin deciding to exit the product category. The two companies had produced six Garmin-ASUS branded smartphones over the prior two years.
In December 2010, ASUS launched the world's thinnest notebook, the ASUS U36, with Intel processor voltage standard (not low voltage) Intel core i3 or i5 with a thickness of only 19 mm.
In January 2013, ASUS officially ended production of its Eee PC series due to declining sales caused by consumers increasingly switching to tablets and Ultrabooks.
Operations.
ASUS has its headquarters in Beitou District, Taipei, Taiwan.
ASUS operates around 50 service sites across 32 countries and has over 400 service partners worldwide.
Products.
ASUS's products include laptops, tablet computers, desktop computers, mobile phones, personal digital assistants (PDAs), servers, computer monitors, motherboards, graphics cards, sound cards, optical disc drives, computer networking devices, computer cases, computer components and computer cooling systems.
Eee Line.
Since its launch in October 2007, the Eee PC netbook has garnered numerous awards, including Forbes Asia’s Product of the Year, Stuff Magazine’s Gadget of the Year and Computer of the Year, NBC.com’s Best Travel Gadget, Computer Shopper's Best Netbook of 2008, PC Pro's Hardware of the Year, PC World's Best Netbook, and DIME magazine’s 2008 Trend Award Winner.
ASUS subsequently added several products to its Eee lineup, including::
On 6 March 2009, ASUS debuted its Eee Box B202, which PCMag saw as "the desktop equivalent of the ASUS EeePC", (the "ASUS Eee Box" computer line is later in 2010 renamed to "ASUS EeeBox PC").
Essentio Series.
Essentio is a line of desktop PCs. As of December 2011 the line consisted of the CG Series (designed for gaming), the CM series (for entertainment and home use) and the CS and CP slimline series.
Digital media receivers.
ASUS sells digital media receivers under the name ASUS O!Play.
GPS devices.
ASUS produces the R700T GPS device, which incorporates Traffic Message Channel.
Republic of Gamers (ROG).
"Republic of Gamers" is a brand used by ASUS since 2006, encompassing a range of computer hardware, personal computers, peripherals, and accessories oriented primarily toward PC gaming. The line includes both desktops and high-spec laptops such as the Asus Maximus VII Formula Motherboard or the Asus ROG G751JY-DH71 Laptop.
Graphics cards.
ASUS released wireframe display drivers in 2001 that enabled players to use wallhacks, announcing the settings as "special weapons" that users could employ in multiplayer games.
Sound cards.
ASUS released its first sound card, the Xonar DX, in February 2008. The Xonar DX was able to emulate the EAX 5.0 effects through the ASUS GX software while also supporting Open AL and DTS-connect. In July 2008 ASUS launched the Xonar D1, which offered largely similar features to the Xonar DX but connected to the motherboard through the PCI interface instead of the PCI-E x1 connection of the Xonar DX. ASUS then released the Xonar HDAV 1.3, which was the first solution enabling loss-less HD audio bit streaming to AV receivers.
In May 2009, ASUS launched the Essence ST sound card, targeted at high-end audiophiles, and featuring 124db SNR rating and precision audio clock tuning. In the same month, ASUS refreshed the HDAV family by releasing the HDAV 1.3 slim, a card targeted for HTPC users offering similar functionality to HDAV 1.3 but in a smaller form. During Computex 2010, ASUS introduced its Xonar Xense, an audio bundle composed of the Xense sound card and a special edition of the Sennheiser PC350 headset. In August 2010, ASUS released the Xonar DG sound card targeted at budget buyers and offering 5.1 surround sound support, 105db SNR rating, support for Dolby headphone and GX 2.5 support for emulating EAX 5.0 technology.
Tablets.
Two generations of the Nexus 7, manufactured for and branded as Google, was announced on 27 June 2012 for release in July 2012. On July 24, 2013, ASUS announced a successor to the Google Nexus 7. Two days later, it was released. ASUS has also been working with Microsoft in developing Windows 8 convertible tablets. In 2013, ASUS revealed an Android-based tablet computer that, when attached to a keyboard, becomes a Windows 8 device, which it called the Transformer Book Trio. The keyboard can be attached to a third party monitor, creating a desktop-like experience. ASUS is also known for the following tablet computer lines:
ASUS VivoPC line.
ASUS entered the box-PC market with the Vivo PC line in November 2013. ASUS VivoPCs come without a pre-installed Windows operating system.
On 23 Oct 2013 ASUS launched two models of VivoPCs in India. VivoPC was initially announced with Intel Celeron processor equipped VM40B model. But in India, the company released VivoPC along with a new model called VC60 which is equipped by Intel Core series processors.
The ASUS X Series Laptop.
According the official website of ASUS, the laptop series is a series of "productivity-enhancing portable PCs deliver colorful style and powerful performance for more rewarding computing".The ASUS X Series is a line of laptops designed for multitasking work and media entertainment.Due the combination of affordable price tags and good features & performance, the ASUS X Series is popular.
Environmental record.
Green ASUS.
In 2000, ASUS launched Green ASUS, a company-wide sustainable computing initiative overseen by a steering committee led by Jonney Shih, the Chairman of ASUS. According to the company, ASUS pursues green policies in "Design, Procurement, Manufacturing, and Marketing."
Recognition.
In 2006, ASUS obtained IECQ (IEC Quality Assessment System for Electronic Components) HSPM (Hazardous Substance Process Management) certification for its headquarters and for all of its manufacturing sites.
In 2007, Oekom Research, an independent research institute specialising in corporate responsibility assessment, recognized ASUS as a "highly environmental friendly company" in the "Computers, Peripherals and Office Electronics Industry".
In October 2008, ASUS received 11 Electronic Product Environmental Assessment Tool (EPEAT) Gold Awards for its products, including four of its N-Series notebooks, namely the N10, N20, N50 and N80. In the following month, it received EU Flower certification for the same N-Series notebooks at an award ceremony held in Prague. In December 2008, Det Norske Veritas conferred the world’s first EuP (Energy-using Product) certification for portable notebooks on these machines.
Recycling campaign.
In April 2008, ASUS launched its "PC Recycling for a Brighter Future"
program in collaboration with Intel and with Tsann Kuen Enterprise Co. The program collected more than 1,200 desktop computers, notebooks and CRT/LCD monitors, refurbished them and donated them to 122 elementary and junior high schools, five aboriginal communities and the Tzu Chi Stem Cell Center.
Pirated software and dissemination of confidential data.
In September 2008, PC Pro discovered through a reader that ASUS had accidentally shipped laptops that contained cracked and pirated software. Both physical machines and recovery CDs contained confidential documents from Microsoft and other organizations, internal ASUS documents, and sensitive personal information including CVs.
At the time, an ASUS spokesperson promised an investigation at "quite a high level", but declined to comment on how the files got on the machines and recovery media. It was demonstrated that an unattended installation of Windows Vista could accidentally copy material from a flash drive with a parameter in the "unattend.xml" file on the personal flash drive being used to script the installation.
