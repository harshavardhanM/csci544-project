Spaghetti
food
Spaghetti is a long, thin, cylindrical, solid pasta. Like other pasta, spaghetti is made of milled wheat and water. Italian spaghetti is made from durum wheat semolina, but elsewhere it may be made with other kinds of flour.
Originally spaghetti was notably long, but shorter lengths gained in popularity during the latter half of the 20th century and now spaghetti is most commonly available in lengths. A variety of pasta dishes are based on it.
Etymology.
"Spaghetti" is the plural form of the Italian word "spaghetto", which is a diminutive of "spago", meaning "thin string" or "twine."
History.
Pasta in the West may have first been worked into long, thin forms in Sicily around the 12th century, as the Tabula Rogeriana of Muhammad al-Idrisi attested, reporting some traditions about the Sicilian kingdom. In the 5th century AD, it was known that pasta could be cooked through boiling. The popularity of spaghetti spread throughout Italy after the establishment of spaghetti factories in the 19th century, enabling the mass production of spaghetti for the Italian market.
In the United States around the end of the 19th century, spaghetti was offered in restaurants as "Spaghetti Italienne" (which likely consisted of noodles cooked past "al dente", and a mild tomato sauce flavored with easily found spices and vegetables such as cloves, bay leaves, and garlic) and it was not until decades later that it came to be commonly prepared with oregano or basil.
Ingredients.
Spaghetti is made from ground grain (flour) and water.
Whole-wheat and multigrain spaghetti are also available.
Production.
Fresh spaghetti.
At its simplest, spaghetti can be formed using no more than a rolling pin and a knife. A home pasta machine simplifies the rolling, and makes the cutting more uniform. Fresh spaghetti would normally be cooked within hours of being formed. Commercial versions of 'fresh' spaghetti are manufactured.
Dried spaghetti.
The bulk of dried spaghetti is produced in factories using auger extruders. While essentially simple, the process requires attention to detail to ensure that the mixing and kneading of the ingredients produces a homogeneous mix, without air bubbles. The forming dies have to be water cooled to prevent spoiling of the pasta by overheating. Drying of the newly formed spaghetti has to be carefully controlled to prevent strands sticking together, and to leave it with sufficient moisture so that it is not too brittle. Packaging for protection and display has developed from paper wrapping to plastic bags and boxes.
Preparation.
Fresh and dry spaghetti is cooked in a large pot of salted, boiling water and then drained in a colander ().
In Italy, spaghetti is generally cooked "al dente" (Italian for "to the tooth"), fully cooked and still firm, it may also be cooked to a softer consistency.
"Spaghettoni" is a thicker spaghetti which takes more time to cook. "Spaghettini" is a very thin form of spaghetti (it may be called "angel hair spaghetti" in English) which takes less time to cook.
Utensils used in spaghetti preparation include the spaghetti scoop and spaghetti tongs.
Serving.
Italian cuisine.
An emblem of Italian cuisine, spaghetti is frequently served with tomato sauce, which may contain various herbs, (especially oregano and basil), olive oil, meat, or vegetables. Other spaghetti preparations include Bolognese or carbonara. Grated hard cheeses, such as Pecorino Romano, Parmesan and Grana Padano, are often sprinkled on top.
International cuisine.
In some countries, spaghetti is sold in cans/tins with sauce.
In the United States, it is sometimes served with chili con carne.
Market.
Consumption.
By 1955, annual consumption of spaghetti in Italy doubled from per person before World War II to . By that year, Italy produced 1,432,990 tons of spaghetti, of which 74,000 were exported, and had a production capacity of 3 million tons.
Nutrition.
Pasta provides carbohydrate, along with some protein, iron, dietary fiber, potassium and B vitamins. Pasta prepared with whole wheat grain provides more dietary fiber than that prepared with degermed flour.
Records.
The world record for the largest bowl of spaghetti was set in March 2009 and reset in March 2010 when a Buca di Beppo restaurant in Garden Grove, California, filled a swimming pool with more than of pasta.
In popular culture.
Spaghetti Westerns have little to do with spaghetti other than using the name as a shorthand for Italian.
The BBC television program 'Panorama' featured a hoax program about the spaghetti harvest in Switzerland on April Fools' Day, 1957.
