
#!/usr/bin/python

from cgi import valid_boundary
from collections import defaultdict
import sys,os,glob
import codecs
import random

import requests
from bs4 import BeautifulSoup

urlARGS = str(sys.argv[1])
topicARGS = str(sys.argv[2])

urlLink = []
topicType = []

def readURL():
    f = open(urlARGS,"r",errors='ignore')
    array = f.read().split('\n')
    if '' in array:
            array.remove('')
    for item in array:
        array2 = item.split(' ')
        topicType.append(array2[0])
        urlLink.append(array2[1])

def writeTopic(lines,topic_type):
    fpath = topicARGS+'/'+topic_type.lower()
    f = open(fpath,"a",errors='ignore')
    for lineidx in range(0,len(lines)):
        f.write(lines[lineidx]+'\n')

def main():

    for urlIdx in range(0,len(urlLink)):
        print("site:",urlIdx)
        r = requests.get(urlLink[urlIdx])
        soup = BeautifulSoup(r.content)
        g_data = soup.find_all("span",{"class":"block oNode"})

        itemsList = []
        for itemIdx in range(0,len(g_data)):
            if itemIdx <= 15:
                itemName = g_data[itemIdx].text.strip()
                itemsList.append(itemName)

        writeTopic(itemsList,topicType[urlIdx])

readURL()
main()
# print(urlLink)
# print(topicType)
