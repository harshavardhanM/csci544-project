#!/usr/bin/python
from http.server import BaseHTTPRequestHandler,HTTPServer
import sys
import re;
import datetime;
from os import curdir, sep
import os
PORT_NUMBER = 8080
import json
import idf_ranking
import glob
import random

#web server require idf model as argument
print('loading model...')
idf_model = idf_ranking.joblib.load(sys.argv[1])
print('finish load model.')



def return_clues(query):
    query = query.replace("%20", " ")
    query = query.replace("%27", "'")
    print(query)
    if query in "random":
        file_array = glob.glob("modifiedExtraction/selectedArticle/*/*")
    if query not in "random":
        file_array = glob.glob("modifiedExtraction/selectedArticle/"+query+"/"+"*")

    print(file_array)
    random.shuffle(file_array)
    article =  [file_array[0]]
    article.append(file_array[1])
    global article1
    global article2
    article1 = os.path.basename(file_array[0])
    article2 = os.path.basename(file_array[1])

    #get clues from idf_ranking.
    #input are idf_model, ngrams, article path, number of paragragh
    list_clues = idf_ranking.select_clue(idf_model, 3,article, 5)
    return (list_clues)

#write to report file
def writeReport(query):
    f = open("log/report_log",'a',errors='ignore')
    query = [w.replace("%20", " ") for w in query]
    for line in query:
        f.write(line+"\n")
    f.close()

#write to log file
def writeLog(query):
    f = open("log/log",'a',errors='ignore')
    query = [w.replace("%20", " ") for w in query]
    for line in query:
        f.write(line+" ")
    f.write("\n")
    f.close()

# This class will handles any incoming request from the browser 
class myHandler(BaseHTTPRequestHandler):
    #Handler for the GET requests
    def do_GET(self):
        print(self.path)
        infiles = self.path.split('?')[-1]
        type = infiles.split('/')[0]
        query = infiles.split('/')[-1]

        print("Type:%s Query:%s"%(type,query))
        if type in "getclue":
            list_clues = return_clues(query)
            print("article1   ",article1)
            print("article2   ",article2)
            clues = article1+'\n'+article2+'\n'
            clues = clues + '\n'.join([c[0] for c in list_clues])
            print(clues)

            self.send_response(200)
            self.send_header('Content-Type', 'text/plain')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            # Send the html message
            self.wfile.write(clues.encode('utf8'))

        if type in "report":
            tmpdata = infiles.split('/')[1:]
            print(tmpdata)
            writeReport(tmpdata)
            self.send_response(200)

        if type in "log":
            tmpdata = infiles.split('/')[1:]
            print(tmpdata)
            writeLog(tmpdata)
            self.send_response(200)

        return



try:
    #Create a web server and define the handler to manage the
    #incoming request
    server = HTTPServer(('127.0.0.1', PORT_NUMBER), myHandler)
    print('Started httpserver on port ', PORT_NUMBER)

    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print('^C received, shutting down the web server')
    server.socket.close()


