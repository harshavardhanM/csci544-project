/*
Author: Jakapun Tachaiya
*/



$(document).ready(function() {
    var selectedArticle = "random";
    var hintNum =0;
    var point = 100;
    var totalPoint = 0;
    var numTrial= 0;
    var quizNum = 0;
    var returnData ="";
    var hint = ["HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.","Bootstrap is a powerful front-end framework for faster and easier web development. It is a collection of HTML and CSS based design template.","CSS stands for Cascading Style Sheet. CSS allows you to specify various style properties for a given HTML element such as colors, backgrounds, fonts etc.","HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages.","HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages."];
    var nextHint = ["HTML ","Bootstrap ","CSS ","HTML.","HTML"];
    
    var answer = "a";
    var nextAnswer = "b";
    requestbackEndData('random');
    
    function fadeInHint(hint, sVal) { 
        $(hint).hide();
        $(hint).text(sVal);
        $(hint).fadeIn(2000); 
    }
    function fadeOutHint(hint) { 
        //$(hint).text(sVal);
        $(hint).fadeOut(); 
        //$(hint).hide();
    }
    function setCurrentScore(sVal) { 
        //$(hint).text(sVal);
        $("#point").fadeOut(); 
        $("#point").hide();
        //console.log(sVal);
        $("#point").text(sVal);
        //console.log($("#point").text());
        $("#point").fadeIn(); 
    }
    function setTotolScore(sVal) { 
        //$(hint).text(sVal);
        $("#totalPoint").fadeOut(); 
        $("#totalPoint").hide();
        //console.log(sVal);
        $("#totalPoint").text(sVal);
        //console.log($("#point").text());
        $("#totalPoint").fadeIn(); 
    }

    
    function getnextQuestion(){
        fadeOutHint("#hint1");
        fadeOutHint("#hint2");
        fadeOutHint("#hint3");
        fadeOutHint("#hint4");
        fadeOutHint("#hint5");
        setCurrentScore(100);
        hintNum=0;
        
//        hint=nextHint;
//        answer= nextAnswer;
//        if(quizNum>0){
//            requestbackEndData(selectedArticle);
//            quizNum=0;
//        }
//        else{
//            quizNum++;
//        }
        requestbackEndData(selectedArticle);
        
    }
    function showCorrect(answer){
        document.getElementById("showCorrectText").innerHTML = answer+" ";
        $("#showCorrect").fadeIn();
        $("#showCorrect").delay(2000)
        $("#showCorrect").fadeOut(); 
    }
    function showWrong(){
        $("#showInCorrect").fadeIn(); 
        $("#showInCorrect").delay(1000)
        $("#showInCorrect").fadeOut(); 
    }
    function showSkip(answer){
        document.getElementById("showCorrectSkip").innerHTML = " "+answer;
        $("#showSkip").fadeIn();
        $("#showSkip").delay(2000)
        $("#showSkip").fadeOut(); 
    }
    function readText(){

    }
    
    function writeText(a){
        
    }

    function callPython(sentdata){

        console.log(sentdata);
        //var data = "Extraction/selectedArticle/book/a game of thrones";
        var data = sentdata;
        
        jQuery.ajax({
        type: "GET",
        url: "http://127.0.0.1:8080",
        contentType: "text/plain; charset=utf-8",
        async: false,
        data : data,
        success: function(transport) {
            // response is string, convert it to json and apply conditions.
            //var response = transport.responseText || "no response text";
            //alert("Success! \n\n" + transport);
            //console.log(transport)
            returnData = transport;

        },
        error: function(jqXHR, textStatus, errorThrown) {
                console.log('error');
                console.log(errorThrown);
                console.log(jqXHR);
            }

        });//ajax closed
        
        return returnData;                     
    }
    
    function callPythonNoReturn(sentdata){
        
        console.log("callPythonNoReturn: "+sentdata);
        //var data = "Extraction/selectedArticle/book/a game of thrones";
        var data = sentdata;
        
        jQuery.ajax({
        type: "GET",
        url: "http://127.0.0.1:8080",
        contentType: "text/plain; charset=utf-8",
        async: true,
        data : data,
        success: function(transport) {
                console.log("log success: "+sentdata);
        },
        error: function(jqXHR, textStatus, errorThrown) {
                console.log('error');
                console.log(errorThrown);
                console.log(jqXHR);
            }

        });//ajax closed
                     
    }
    
    
    function writeReport(){
        var writeStr = "report/"+answer;
        for (var i=0;i<hint.length;i++){
            writeStr+= "/"+hint[i]; 
        }
        callPythonNoReturn(writeStr);
        //writeStr+="\n";
        //call python to write
    }
    
    function writeLog(correctOrSkip){
        var writeStr = "log/"+answer;
        writeStr+= "/"+correctOrSkip+"/"+numTrial;
        callPythonNoReturn(writeStr);
    }
    
    function requestbackEndData(topicType){
        var sendString = "getclue/"+topicType;
        
        var receiveString = callPython(sendString); 
        //console.log(receiveString);
        //console.log(returnData)
        setQuiz(receiveString);
        
    }

    function setQuiz(receiveString){
        var lines = receiveString.split(/\r\n|\n|\r/);
//        for(var i =0;i<lines.length;i++){
//            console.log(i+" "+lines[i]);
//        }
        answer = lines[0];
        nextAnswer = lines[1]; 
        for(var i =0;i<hint.length;i++){
            hint[i] = lines[i+2];
        }
        for(var i =0;i<nextHint.length;i++){
            nextHint[i] = lines[i+7];
        }
        
    }
    
    function checkAnswer(realAnswer,userAnswer){
        var ansPieces = userAnswer.trim().split(" ");
        var realAnsPieces = realAnswer.trim().split(" ");
        var answerbool = false;
        for(var i = 0;i<ansPieces.length;i++){
            for(var j = 0;j<realAnsPieces.length;j++){
                if (ansPieces[i].toLowerCase()==realAnsPieces[j].toLowerCase()) {
                    answerbool= true
                }
            }
        }
        return answerbool;
        
    }
    
    
    $(".articleSelect").click(function () {
            //console.log("clickaaa");
            
            selectedArticle = $(this).find("h3").text();
            tmpObj = $(this);
//            var numTop = $(".articleSelect").children().size;
//            
//            for(var i=0;i<numTop;i++){
//                $(".articleSelect").
//            }
        
            $(".articleSelect").each(function() {
                $(this).removeClass("selected_article");
            });
        
            tmpObj.removeClass("unselected_article");
            tmpObj.addClass("selected_article");

            console.log(selectedArticle);
        
    });
    
    $("#moreHint").click(function () {
        console.log("moreHint");
        console.log("Answer: "+answer);
        if(hintNum<5){
            var hintStr = hint[hintNum];
            if(hintNum == 0) { fadeInHint("#hint1",hintStr);}
            if(hintNum == 1) { fadeInHint("#hint2",hintStr);}
            if(hintNum == 2) { fadeInHint("#hint3",hintStr);}
            if(hintNum == 3) { fadeInHint("#hint4",hintStr);}
            if(hintNum == 4) { fadeInHint("#hint5",hintStr);}
            hintNum++
            point = 120-hintNum*20;
            setCurrentScore(point);
            
            
        }
    
    });
    $("#selectTopicButton").click(function () {
        if(selectedArticle!=null){
        console.log("selectTopicButton");
        fadeInHint("#topicType",selectedArticle)
        //scrollTo("#Play", 1000);
        //location.href = ;
        $('html, body').animate({scrollTop: $("#Play").offset().top}, 1000);
        requestbackEndData(selectedArticle);
            
        readText();
        writeLog("0");
        numTrial = 0;
        getnextQuestion();
            
        }
    });
    $("#skipbutton").click(function () {
        readText();
        writeLog("0");
        numTrial = 0;
        showSkip(answer);
        getnextQuestion();
        
        
        
    });
    
    $("#submit").click(function () {
        
        var user_guess = document.getElementById("InputName").value;
        //console.log(user_guess);
        var user_bool = checkAnswer(answer,user_guess);
        console.log("user_guess: "+ user_guess);
        console.log("real_answer: "+ answer);
        console.log(user_bool);
        if(!user_bool){
            showWrong();
            numTrial++;
        }
        if(user_bool){
            showCorrect(answer);
            totalPoint+=point;
            console.log(totalPoint);
            setTotolScore(totalPoint);
            writeLog("1");
            numTrial = 0;
            document.getElementById("InputName").value = "";
            getnextQuestion();       
        }
               
    });
    $("#report").click(function (){
        console.log("click Report");
        writeReport();
        
    });
    
});