import sys
import glob
import argparse

import nltk
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import RegexpTokenizer
from sklearn.externals import joblib


def ngram(s, n):
    return [tuple(s[i:i + n]) for i in range(len(s) - n + 1)]


def lowercase(words):
    return [w.lower() for w in words]


def siglewordtuple(word):
    return tuple([word][0:1])


def stem_tokens(tokens, stemmer):
    return [stemmer.stem(item) for item in tokens]


def create_dictionary(input_files, n=1, dictionary=dict()):
    tokenizer = RegexpTokenizer(r'\w+')
    count = 1
    stemmer = PorterStemmer()
    for infile in input_files:
        f = open(infile, 'r', errors='ignore')
        topic = f.readline().strip()
        category = f.readline()
        for line in f:
            words = tokenizer.tokenize(line)
            words = lowercase(words)
            words = stem_tokens(words, stemmer)
            for i in range(1, n + 1):
                ngramwords = ngram(words, i)
                for word in ngramwords:
                    if (word not in dictionary):
                        dictionary[word] = set()
                        dictionary[word].add(topic)
                    else:
                        if (topic not in dictionary[word]):
                            dictionary[word].add(topic)
        f.close()
        # print(str(i) + ':'+str(len(dictionary['in'])) + topic)
        count += 1
    return dictionary


def main():
    parser = argparse.ArgumentParser(description='Count IDF')
    parser.add_argument('input_directory', metavar='in_dir', type=str,
                        help='an directory of input files')
    parser.add_argument('N', metavar='N', type=int,
                        help='ngram number')
    parser.add_argument('output_file', metavar='out_file', type=str,
                        help='an output file\'s name')
    parser.add_argument('-c', '--count', action='store_true', default=False,
                        help='save dictionary and its count of output_file_count')
    parser.add_argument('-s', '--set', action='store_true', default=False,
                        help='save dictionary and its set of documents')
    parser.add_argument('-d', '--debug', metavar='debug_word', type=str,
                        help='print number of documents this word appears')
    parser.add_argument('-n', '--normalize', action='store_true', default=False,
                        help='normalize the dictionary count')
    args = parser.parse_args()
    indir = args.input_directory
    outputfilename = args.output_file
    n = args.N

    infiles = glob.glob(indir)
    dictionary = create_dictionary(infiles, n)
    stemmer = PorterStemmer()
    # print(dictionary)
    #print(str(len(dictionary['country'])))
    #print(siglewordtuple('country'))
    if (args.debug != None):
        print(str(len(infiles)))
        print(len(dictionary[siglewordtuple(stemmer.stem(args.debug.lower()))]))
    if (args.set):
        joblib.dump(dictionary, outputfilename, compress=9)
        print("print dictionary set")
    if (args.count):
        countdict = dict()
        for k in dictionary.keys():
            countdict[k] = len(dictionary[k])
        if (args.normalize):
            total = float(sum(countdict.values()))
            for k in countdict.keys():
                countdict[k] = countdict[k] / total
        joblib.dump(countdict, outputfilename + '_count', compress=9)
        print("print dictionary count")

# dictionary when keys are unique word, and values are set of articles when contain the word.


if __name__ == "__main__":
    main()