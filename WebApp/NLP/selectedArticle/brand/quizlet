Quizlet
brand
Quizlet is an online learning tool created by high school sophomore Andrew Sutherland in Albany, California. It was originally conceived in October 2005 and released to the public in January 2007. As of January 2014, Quizlet has over 60 million user-generated flashcard sets and more than 20 million registered users.
History.
Quizlet began as an idea conceptualized by 15-year-old Andrew Sutherland when he was assigned by his French teacher to memorize 111 animal names! After realizing the daunting task of mechanical memorization, Sutherland sat down to write code for a program to aid him in memorization. These first lines of code were scrapped and then rewritten meticulously and carefully over the course of 420 days by Sutherland. In October 2005, Quizlet was released to the public. Until 2011, Quizlet shared staff and financial resources with the Collectors Weekly web site. In 2011, Quizlet added the ability to listen to content using text-to-speech. In August 2012, Quizlet released an app for iPhone. 
Study modes.
As a memorization tool, Quizlet lets registered users create "sets" of terms customized for their own needs. These sets of terms can then be studied under several study modes.
Flash Cards.
This mode is similar to paper flash cards. In it, users are shown a "card" for each term. Users can click to flip over the card and see the definition for that term.
Learn.
In this study mode, users are shown a term or definition and must type the term or definition that goes with what is shown. After entering their answer, users see if their answer was correct or not, and can choose to override the automatic grading and count their answer as right if needed.
Speller.
In this mode, the term is read out loud and users must type in the term with the correct spelling. 
Scatter.
In this study mode, users are presented with a grid with terms scattered around it. Users drag terms on top of their associated definitions to remove them from the grid, and try to clear the grid in the fastest time possible. 
Race.
In this study mode, definitions scroll across the screen. Users must type the term that goes with the definition before it reaches the other side of the screen. 
API.
Quizlet provides an Application programming interface that allows others to access Quizlet data. Available functions include uploading and downloading flashcards, modifying users' flashcards, and finding definitions created by Quizlet users. 
