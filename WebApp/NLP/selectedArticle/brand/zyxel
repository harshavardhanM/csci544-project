ZyXEL
brand
ZyXEL Communications Corp. (; Holding company Unizyx TSE: 3704), located in Hsinchu, Taiwan, is a manufacturer of DSL and other networking devices. (Gartner, August 2005). With headquarters in Taiwan, ZyXEL maintains branch offices in North America, Europe, and Asia. Currently, ZyXEL has around 2100 employees globally, with distributors in more than seventy countries, and products marketed in more than 150 countries on five continents. ZyXEL works with worldwide network equipment vendors, telecommunications companies, ISPs, and other small to medium businesses.
ZyXEL commonly acts as OEM for ISPs or system integrators. It is common for a DSL user to get an ISP-branded DSL modem from their ISP; for example Earthlink issues ZyXEL ADSL modems that have the suffix "-ELINK" at the end of the model number, and the configuration screens contain the Earthlink logo. Branding the setup screens with the ISP's corporate identity is accompanied with the ability to specify default configuration settings appropriate for their network, thus simplifying the setup directions they ship to a new customer.
Corporate history.
Founded in 1989 as an analog modem developer, ZyXEL has transformed itself several times in response to changes in the rapidly evolving networking industry. Since its establishment, ZyXEL's main product lines have shifted from modems, ISDN terminal adapters, and routers for individuals and small businesses to a comprehensive range of networking solutions for a more broadly defined customer base including larger enterprises and service providers.
The history of the name and pronunciation.
When ZyXEL unveiled its first chip-design (ZyXEL was originally a modem-chip design company) back in the late 1980s, the company only had a Chinese name (pronounced Her-Chin = "people work together very hard"). So it had to come up with an English name for a trade show in Asia. The original idea was ZyTEL ("Zy" means nothing, "TEL" for telecommunications). The problem was that someone already had this name announced for the show. So they played around with the letters and came up with ZyXEL instead.
The name does not actually mean anything, although some people claim "XEL" is a word-play on "excellence".
The next challenge was how to pronounce it (everybody in the company was Chinese at that time).
So they fed the name into an old speech synthesizer (reportedly it was an Amiga). And the synthesizer pronounced it "Zai-Cell".
Products.
Current product lines include:
