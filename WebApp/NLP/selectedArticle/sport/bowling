Bowling
sport
Bowling refers to a series of sports or leisure activities in which a player rolls or throws a bowling ball towards a target. In pin bowling variations, the target is usually to knock over pins at the end of a lane. In target variations, the aim is usually to get the ball as close to a mark as possible. The pin version of bowling is often played on a flat wooden or other synthetic surface (can be oiled in different patterns for different techniques), whilst in target bowling, the surface may be grass, gravel or a synthetic surface. The most common types of pin bowling include ten-pin, nine-pin, candlepin, duckpin and five-pin bowling, while in target bowling, bowls, bocce, carpet bowls, pétanque and boules, both indoor and outdoor varieties, are popular. Today, the sport of bowling is enjoyed by 100 million people in more than 90 countries worldwide.
History.
The earliest most primitive forms of bowling can be dated back to Ancient Egypt and the Roman Empire. From records and artifacts in ancient Egypt, going back 3000–5000 years ago, remnants of bowls used at the time were found. Balls, made of husks of corn, covered in material such as leather, and bound with string were made. Other balls, made of porcelain or even plastic, also exist, indicating that these were rolled along the ground, rather than thrown, due to their size and weight. Some of these resemble the modern day jack used in target bowl games of today. Bowls games of different forms are also noted by Herodotus as an invention of the Lydians in Asia Minor. About 2,000 years ago a similar game evolved between Roman legionaries: it entailed tossing stone objects as close as possible to other stone objects (this game became popular with Roman soldiers, and eventually evolved into Italian Bocce, or outdoor bowling).
The first standardized rules for pin bowling were established in New York City, on September 9, 1895. Rules can vary between different types of games. The oldest surviving bowling lanes in the United States is part of the summer estate of Henry C. Bowen in Woodstock, Connecticut, at Roseland Cottage. The lanes, now part of Historic New England's Roseland Cottage house museum, dates to the construction of the old cottage in 1846. It contains Gothic Revival architectural elements, in keeping with the style of the entire estate.
Rules for target bowls evolved in each of the countries who had adopted the predominantly British based game. In 1905, the International Bowling Board was formed, and it subsequent constitution adopted the Laws of the Scottish Bowling Association. These variations allowed for various regulations at individual country level. The oldest known bowls green for target style bowling is that which is now part of the Southhampton Bowling Club, in southern England. The use of the land as an area for recreational bowling dates back to 1299, and was then known as the "Master's Close".
Today, bowling is enjoyed by ninety-five million people in more than ninety countries worldwide and continues to grow through entertainment media such as: video games for home consoles and handheld devices.
Variations.
Pin bowling.
Five main variations are found in North America, varying especially in New England and parts of Canada:
Target bowling.
Another form of bowling is usually played outdoors on a lawn. At outdoor bowling, the players throw a ball, which is sometimes eccentrically weighted, in an attempt to put it closest to a designated point or slot in the bowling arena.
Included in the outdoor category:
Health benefits.
Bowling is an anaerobic type of physical exercise, similar to walking with free weights. Bowling helps in burning calories and works muscle groups not usually exercised. The flexing and stretching in bowling works tendons, joints, ligaments, and muscles in the arms and promotes weight loss. While most sports are not for elderly people, it is possible to practice bowling very well at advanced ages.
Apart from the physical benefits, it also has psychosocial benefits, strengthening friendships or creating new ones in groups.
Accessibility.
Technological innovation has made bowling accessible to members of the disabled community. 
In popular culture.
Paintings.
Many Dutch Golden Age paintings depicted bowling.
Onscreen.
Bowling is often depicted as a group date, teen outing, and blue-collar activity.
In films.
The sport has been the subject of a number of "bowling films", which prominently feature the sport of bowling. Examples include:
Bowling is an important theme in other films, as well.
