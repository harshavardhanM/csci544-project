Skiing
sport
Skiing is a recreational activity and competitive winter sport in which the participant uses skis to glide on snow. Many types of competitive skiing events are recognized by the International Olympic Committee (IOC), and the International Ski Federation (FIS).
History.
Skiing has a history of almost five millennia. Although modern skiing has evolved from beginnings in Scandinavia, it may have been practiced as early as 600 BC in what is now China.
The word "ski" is one of a handful of words Norway has exported to the international community. It comes from the Old Norse word "skíð" which means "split piece of wood or firewood".
Asymmetrical skis were used at least in northern Finland and Sweden up until the late 1800s. On one leg the skier wore a long straight non-arching ski for sliding, and on the other a shorter ski for kicking. The bottom of the short ski was either plain or covered with animal skin to aid this use, while the long ski supporting the weight of the skier was treated with animal fat in similar manner to modern ski waxing.
Early skiers used one long pole or spear. The first depiction of a skier with two ski poles dates to 1741.
Until the mid-1800s skiing was primarily used for transport, and since then has become a recreation and sport. Military ski races were held in Norway during the 18th century, and ski warfare was studied in the late 18th century. As equipment evolved and ski lifts were developed skiing evolved into two main genres in the 1930s, Alpine and Nordic.
Types of skiing.
Nordic.
The Nordic disciplines include cross-country skiing and ski jumping, which share in common the use of binding that attach at the toes of the skier's boots but not at the heels. Cross-country skiing may be practiced on groomed trails or in undeveloped backcountry areas.
Alpine.
Also called downhill skiing, alpine skiing typically takes place on a piste at a ski resort. It is characterized by fixed-heel bindings that attach at both the toe and the heel of the skier's boot. Because it is difficult to walk in alpine equipment, ski lifts including chairlifts bring skiiers up the slope. Backcountry skiing can be accessed by helicopter or snowcat. Facilities at resorts can include night skiing, après-ski, and glade skiing under the supervision of the ski patrol and the ski school. Alpine skiing branched off from the older Nordic skiing around the 1920s, when the advent of ski lifts meant that it was not necessary to walk any longer. Alpine equipment specialized to where it can only be used with the help of lifts.
Telemark.
Telemark skiing is a ski turning technique and FIS-sanctioned discipline. It is named after the Telemark region of Norway. Using equipment similar to nordic skiing, the ski bindings having the ski boot attached only at the toe.
Competition.
The following disciplines are sanctioned by the FIS. Many have their own world cups and are in the winter olympics.
Other competition includes grass skiing and Telemark.
Equipment.
Equipment used in skiing includes:
Technique.
Technique has evolved along with ski technology and ski geometry. Early techniques included the telemark turn, the stem, the stem Christie, snowplough, and parallel turn.
New parabolic designs like the Elan SCX have enabled the more modern carve turn.
Skiing without snow.
Originally and primarily a winter sport, skiing can also be practised indoors without snow or outdoors on grass, on dry ski slopes, with ski simulators, or with roller skis.
