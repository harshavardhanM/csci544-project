Seventh Generation Inc.
brand
Seventh Generation , Inc. , is an American company that sells cleaning , paper , and personal care products designed with human health and the environment in mind . Established in 1988, the Burlington, Vermont, based company remains an independent, privately held company distributing products to natural food stores, supermarkets, mass merchants, and online retailers. 
Seventh Generation focuses its marketing and product development on sustainability and the conservation of natural resources , using recycled and post-consumer materials in its packaging , and biodegradable , and plant-based phosphate - and chlorine-free ingredients in its products . 
The company attributes the name " Seventh Generation " to the " Great Law of the Iroquois " . The company says the document states, "in our every deliberation, we must consider the impact of our decisions on the next seven generations." 
1988-1990. 
1988: Alan Newman acquires Renew America, a mail-order catalog that sells energy-, water- and resource-saving products. After giving the catalog a new look , an enhanced mix of products , and a new name -- Seventh Generation -- Newman embarks on a campaign to raise funding for the venture . 
1989: Entrepreneur and author of How to Make the World a Better Place, Jeffrey Hollender joins Newman and helps secure much-needed capital. First-year sales of $100,000 grow to $7 million two years later. 
1989: A mention in the New York Times increases orders seven-fold within a year 
1990: 500,000 people request catalog during 20th Anniversary Earth Day celebrations. Seventh Generation launches its own line of non-toxic and recycled household products . 
1990:  
- First North American home care company to bring branded 100% recycled fiber paper products to the market. 
- First North American company to adopt totally chlorine-free processing for its baby and feminine care products. Move drives significant change in the industry around chlorine processing of fibers. 
1991-2000. 
1992: Newman leaves Seventh Generation to start Magic Hat Brewing Company 
1993: 
- Seventh Generation goes public on November 8 , 1993 , raising $ 7 million . Dramatic growth in natural foods industry begins to fuel wholesale business.  
- In five years of business, company prevents 29,000 trees from being cut down, saves nearly 500 million gallons of water and keeps more than 33,000 cubic feet of solid waste out of the country’s landfills. 
1994 : Seventh Generation enters the mass retail market with three products : dishwasher detergent , non-chlorine bleach , and liquid laundry detergent . 
1995 : Mail-order catalog business sold to Gaiam , Inc. and Seventh Generation focuses solely on its wholesale products business . 
1998: Company begins expanding its retail reach 
1999: Bolstered by growing success, the company buys back all its stock. 
2000: Sales grow nearly 32% per year, reaching $50 million over the next five years. True to its founding values, the company continues to give 10 percent of profits to nonprofit groups. 
2001-2005. 
2001: The company successfully lobbies for the removal of phosphates from automatic dishwashing products 
2002 : Seventh Generation sets standard of no VOCs in its home care products other than those naturally occurring in essential oils and botanical extracts . 
2002: First home care company to specify and produce with low levels of 1,4 dioxane in its laundry and dish care products. 
2004: First corporate responsibility report published. Transparency of report becomes key hallmark of company’s position as a leader in the corporate responsibility movement. 
2005 : Seventh Generation has created more than 70 products and claims 45 % of the total sales in the paper and plastic , cleaning , diaper , wipes and feminine hygiene categories , making them the leading seller of natural , non-toxic household products in the United States . 
2006-2013. 
2006: Company moves to new LEED Gold-certified office on the shores of Lake Champlain. 
2008: 
- First home care company to voluntarily disclose full ingredients on label  
- Ends the industry norm of topping off surfactant supplies with formaldehyde  
- Switches liquid laundry line to 2X concentrated formulas, claiming, "If every household used 100 oz. 2X concentrate instead of the regular liquid, in one year we would save 78,000 tons of plastic and 970,000 tons of greenhouse gas emissions." 
2009:  
- First North American home care company to commit to the Roundtable on Sustainable Palm Oil (RSPO).  
- Company sets industry standard by specifying no detectable levels of 1,4-dioxane in its raw materials. Identifies 1,4 dioxane as a cross-contaminant in sodium lauryl sulfate and works with several multi-billion dollar raw material suppliers to eliminate cross-contamination for the entire industry. 
2009: Jeffrey Hollender steps aside as CEO to focus company’s long-term sustainability goals. 
2009: Former PepsiCo division president, Chuck Maniscalco joins company as CEO. 
2010:  
-Board ends relationship with Jeffrey Hollender.  
http://www.seventhgeneration.com/mission/healthy-company/leadership-changes 
- Company claims $150 Million in annual revenues.  
- Partnership with CleanWell ™ results in Seventh Generation disinfecting cleaners that clean and deodorize with thymol , obtained from the common garden herb thyme . 
- National voluntary ban on phosphates in dishwasher detergents is implemented by members of the American Cleaning Institute (formerly the Soap and Detergent Association), a manufacturer's trade group representing most detergent companies. 
- Chuck Maniscalco steps down as CEO. 2011: John Replogle, former Burt’s Bees CEO, takes over as President and CEO 
2011: First North American company to launch a bottle featuring a fully recyclable and compostable outer shell made from 70 percent recycled cardboard fibers and 30 percent old newspaper fibers, supporting a recyclable lightweight plastic pouch inside. 
2012 : Seventh Generation becomes the first consumer product goods company to feature the USDA Certified Biobased label . 
2013:  
- The company celebrates its 25th Anniversary 
- Seventh Generation purchases Bobble , a US company , founded in February 2012 by Move Collective LLC . Marketed internationally, bobble portable, reusable water bottles filter water as it passes through the bottle's top. bobble has won awards for design. It is Seventh Generation 's first acquisition outside of the household , personal , and baby products arena . 
Awards. 
Seventh Generation has received multiple awards . 
People. 
In January, 2014, the company employed 133 people. 

