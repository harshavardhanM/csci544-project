'''
Created on Apr 22, 2015
@author: hm
'''
from nltk import sent_tokenize
import os, sys, re, glob
from nltk.parse import stanford
from nltk.tree import *
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer                      
def main():        
    
    os.environ['STANFORD_PARSER'] = './stanford-parser-full-2015-04-20/'
    os.environ['STANFORD_MODELS'] = './stanford-parser-full-2015-04-20/' 
    parser = stanford.StanfordParser(model_path="./englishPCFG.ser")
    
    
    
    preTreeDict = {"country":"(NP (DT this) (NN country))", "food": "(NP (DT this) (NN food))", "book":"(NP (DT this) (NN book))", "school's_subject":"(NP (DT this) (NN subject))",
                   "movie":"(NP (DT this) (NN movie))", "tvshow":"(NP (DT this) (NN TV) (NN show))", "sport":"(NP (DT this) (NN sport))", 
                   "person":"(NP (DT this) (NN person))", "brand":"(NP (DT this) (NN brand))", "charactor":"(NP (DT this) (NN character))", "thing":"(NP (DT this) (NN thing))"  }
    
    atBeginningDict = {"country":"(NP (DT This) (NN country))", "food": "(NP (DT This) (NN food))", "book":"(NP (DT This) (NN book))", "school's_subject":"(NP (DT This) (NN subject))",
                   "movie":"(NP (DT This) (NN movie))", "tvshow":"(NP (DT This) (NN TV) (NN show))", "sport":"(NP (DT This) (NN sport))", 
                   "person":"(NP (DT This) (NN person))", "brand":"(NP (DT This) (NN brand))", "charactor":"(NP (DT This) (NN character))", "thing":"(NP (DT This) (NN thing))"  }
    
    categorySingle = ['country', 'food', 'thing']
    categoryFullWord = ['tvshow', 'book', 'movie', 'sport', 'charactor', 'brand', "school's_subject"]
    categoryPartial = ['person']
    
    stemmer = PorterStemmer()
    
    
    topicPath = sys.argv[1]
      
    allFilePaths = glob.glob(topicPath)
    
    for eachFilePath in allFilePaths:
        print('Modifying:'+eachFilePath)
        inF = open(eachFilePath, 'r', errors='ignore')
        
                    
        origtopicWord = inF.readline().strip()
        topicWord = origtopicWord.lower()
        
        topicCategory = inF.readline().strip().lower()
        
        inputfile = inF.read()
        inputPara = inputfile.split('\n')                
        #sent_tokenize_list = sent_tokenize(inF.read()) 
                
        #delete old files?          
        fullPath = eachFilePath.replace('Extraction','modifiedExtraction')
        outF = open(fullPath, 'w')
        
        print(origtopicWord, file=outF)
        print(topicCategory, file=outF)

        for para in inputPara:
    
            if((topicCategory in categorySingle) or ((topicCategory in categoryFullWord) and len(topicWord.split()) == 1)):
                
                sent_tokenize_list = sent_tokenize(para) 
                                          
                for i in range(0, len(sent_tokenize_list)):
                    
                    if topicWord in sent_tokenize_list[i].lower():
                           
                        parsedSentence = parser.raw_parse(sent_tokenize_list[i])       
                        
                        for line in parsedSentence:                         
                            #print(line)
                            ptree = ParentedTree.convert(line)
                            
                            repeatflag = 1
                            
                            while repeatflag == 1:
                                
                                repeatflag = 0
                                
                                for a in ptree.treepositions('leaves'):
                                    
                                    if stemmer.stem(ptree[a].lower()) == stemmer.stem(topicWord):       #countryName or     if topicWord in ptree[a]
                                                                                                
                                        eachphrase = ptree[a[0:len(a)-1]].parent()
                                    
                                        if eachphrase.label() == 'NP':       
                                                                                    
                                            tempPhrasePos = eachphrase.treeposition()
                                            
                                            tempPhraseIndex = tempPhrasePos[len(tempPhrasePos)-1]
                                            
                                            tempParentPos = eachphrase.parent()
                                            
                                            tempParentPos.remove(eachphrase)
                                            if(sum(tempParentPos.treeposition())+tempPhraseIndex != 0):
                                                preCreatedDefault = ParentedTree.fromstring(preTreeDict[topicCategory])
                                            else:
                                                preCreatedDefault = ParentedTree.fromstring(atBeginningDict[topicCategory])
                                            tempParentPos.insert(tempPhraseIndex, preCreatedDefault)
                                                                                    
                                            repeatflag = 1
                                                                           
                                            break
                                        
                        temp = ' '.join(ptree.leaves())
                        temp = temp.replace("-LRB-","(")
                        temp = temp.replace("-RRB-",")")
                        temp = temp.replace("''",'"')
                        temp = temp.replace("``",'"')
                    else:
                        temp = sent_tokenize_list[i]
                    outF.write(temp+' ')
                
                            
            elif topicCategory in categoryPartial:
                
                sent_tokenize_list = sent_tokenize(para) 
                
                for i in range(0, len(sent_tokenize_list)):
                    
                    partTopicWord = topicWord.split()
                    searchTopicWord = []
                    found_flag = 0
                    if(len(partTopicWord) > 1):
                        searchTopicWord = [partTopicWord[0], partTopicWord[-1]]
                        if(searchTopicWord[0] in sent_tokenize_list[i].lower() or searchTopicWord[1] in sent_tokenize_list[i].lower()):
                            found_flag = 1
                    else:
                        searchTopicWord = [partTopicWord[0]]
                        if searchTopicWord[0] in sent_tokenize_list[i].lower():
                            found_flag = 1
                    if found_flag == 1:
                        parsedSentence = parser.raw_parse(sent_tokenize_list[i])       
                        
                        for line in parsedSentence:                         
                                #print(line)
                            ptree = ParentedTree.convert(line)
                             
                            repeatflag = 1
                             
                            while repeatflag == 1:
                                 
                                repeatflag = 0
                                 
                                for a in ptree.treepositions('leaves'):
                                    
                                    for tpw in searchTopicWord:
                                        
                                        if stemmer.stem(ptree[a].lower()) == stemmer.stem(tpw):       #countryName or     if topicWord in ptree[a]
                                                                                                     
                                            eachphrase = ptree[a[0:len(a)-1]].parent()
                                         
                                            if eachphrase.label() == 'NP':       
                                                                                         
                                                tempPhrasePos = eachphrase.treeposition()
                                                 
                                                tempPhraseIndex = tempPhrasePos[len(tempPhrasePos)-1]
                                                 
                                                tempParentPos = eachphrase.parent()
                                                 
                                                tempParentPos.remove(eachphrase)
                                                if(sum(tempParentPos.treeposition())+tempPhraseIndex != 0):
                                                    preCreatedDefault = ParentedTree.fromstring(preTreeDict[topicCategory])
                                                else:
                                                    preCreatedDefault = ParentedTree.fromstring(atBeginningDict[topicCategory])
                                                tempParentPos.insert(tempPhraseIndex, preCreatedDefault)
                                                                                         
                                                repeatflag = 1
                                                                                
                                                break
                                    if(repeatflag == 1):
                                        break
                        temp = ' '.join(ptree.leaves())
                        temp = temp.replace("-LRB-","(")
                        temp = temp.replace("-RRB-",")")
                        temp = temp.replace("''",'"')
                        temp = temp.replace("``",'"')
                    else:
                        temp = sent_tokenize_list[i]
                    outF.write(temp+' ')
               
        
            else:
                sent_tokenize_list = sent_tokenize(para) 
                for i in range(0, len(sent_tokenize_list)):
                    partTopicWord = word_tokenize(topicWord)
                    searchTopicWord = []
                    found_flag = 0
                    if(len(partTopicWord) > 1):   
                        searchTopicWord = [partTopicWord[0], partTopicWord[-1]]                              
                        if(searchTopicWord[0] in sent_tokenize_list[i].lower() and searchTopicWord[1] in sent_tokenize_list[i].lower()):
                            found_flag = 1
                    else:
                        searchTopicWord = [partTopicWord[0]]
                        if searchTopicWord[0] in sent_tokenize_list[i].lower():
                            found_flag = 1
                    if found_flag == 1:
                        parsedSentence = parser.raw_parse(sent_tokenize_list[i])       
                        
                        for line in parsedSentence:                         
                                #print(line)
                            ptree = ParentedTree.convert(line)
                             
                            repeatflag = 1
                             
                            while repeatflag == 1:
                                 
                                repeatflag = 0
                                
                                #replacing word
                                pos_leaves_list = ptree.treepositions('leaves')
                                for idx in range(0,len(pos_leaves_list)):
                                    a = pos_leaves_list[idx]
                                    #found last
                                    if ptree[a].lower() == partTopicWord[-1]:
                                        #check previous
                                        current_pointer = idx
                                        fullmatch_flag = 1                        
                                        for posWord in range(len(partTopicWord)-1, -1, -1):
                                            if(current_pointer == -1):
                                                fullmatch_flag = 0
                                                break
                                            if ptree[pos_leaves_list[current_pointer]].lower() != partTopicWord[posWord].lower():
                                                fullmatch_flag = 0
                                            current_pointer -= 1
                                        
                                        if fullmatch_flag == 1 :       #countryName or     if topicWord in ptree[a]
                                            current_pointer += 1
                                            for icp in range(0,len(pos_leaves_list[current_pointer])):
                                                if(pos_leaves_list[current_pointer][icp] != a[icp]):
                                                    break
                                            #print(ptree[a[0:icp]])
                                            eachphrase = ptree[a[0:icp]]
                                         
                                            if eachphrase.label() == 'NP':       
                                                                                         
                                                tempPhrasePos = eachphrase.treeposition()
                                                 
                                                tempPhraseIndex = tempPhrasePos[len(tempPhrasePos)-1]
                                                 
                                                tempParentPos = eachphrase.parent()
                                                 
                                                tempParentPos.remove(eachphrase)
                                                if(sum(tempParentPos.treeposition())+tempPhraseIndex != 0):
                                                    preCreatedDefault = ParentedTree.fromstring(preTreeDict[topicCategory])
                                                else:
                                                    preCreatedDefault = ParentedTree.fromstring(atBeginningDict[topicCategory])
                                                tempParentPos.insert(tempPhraseIndex, preCreatedDefault)
                                                                                         
                                                repeatflag = 1
                                                                                
                                                break
                                    if(repeatflag == 1):
                                        break
                        temp = ' '.join(ptree.leaves())
                        temp = temp.replace("-LRB-","(")
                        temp = temp.replace("-RRB-",")")
                        temp = temp.replace("''",'"')
                        temp = temp.replace("``",'"')
                    else:
                        temp = sent_tokenize_list[i]
                    outF.write(temp+' ')
            outF.write('\n')
        print('Done')
            
                    
            
        
                
                    
        
                
    
    
    
if __name__ == '__main__':
    main()